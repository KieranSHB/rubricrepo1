<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the email template
 *
 * @package     local
 * @subpackage  feedback_kboyle
 * @copyright   Kieran Boyle kboyle@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
 */

require_once $CFG->dirroot.'/lib/formslib.php';
require_login();
/*
* This function creates and displays the email form
* It also fills out predefined feedback snippets for the user to enter
* this functionality will be further refined
*/
class create_makeForm_instance extends moodleform{
	function definition(){
	   global $CFG, $DB, $USER;
       //$this->page->requires->js_init_call('M.local_rubricrepo_kboyle.init',array('this is the param1 value'), false, $jsmodule); 
	   $mform = $this ->_form;
       //$this->page->requires->js_init_call('M.local_rubricrepo_kboyle.init',array('this is the param1 value'), false, $jsmodule); 
       //$mfrom->addElement('text', 'formName', get_string('formName', 'local_rubricrepo_kboyle'));
       $mform->addElement('text', 'formName', get_string('formName', 'local_rubricrepo_kboyle'), $attributes);
       $mform->addElement('textarea', 'numCategories', get_string('numCategories', 'local_rubricrepo_kboyle'), 'wrap="virtual" rows="10" cols="60" resize="none" style="resize:none"');
       $repeatarray = array();
       $repeatarray[] = $mform->createElement('text', 'category', get_string('numCategories', 'local_rubricrepo_kboyle'));
       $repeatarray[] = $mform->createElement('text', 'comment', get_string('numpros', 'local_rubricrepo_kboyle'));
 

       $this->add_action_buttons($cancel=true, $sumitlabel = get_string('nextPage', 'local_rubricrepo_kboyle'));

    }

};


?>