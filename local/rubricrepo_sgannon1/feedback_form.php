<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The form used by 'feedback_ec10'
 *
 * @package     local
 * @subpackage  rubricrepo_sgannon1
 * @copyright   Eric Cheng ec10@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
//echo $OUTPUT->get_string('writing', 'local_rubricrepo_sgannon1');
require_once $CFG->dirroot.'/lib/formslib.php';
require_once($CFG->dirroot.'/lib/datalib.php');
require_once('../../config.php');

class create_feedback_instance extends moodleform {
    
    function definition() {
    global $CFG, $DB, $USER;
    $mform = $this->_form;
    $radioarr_w = array();
    //$radioarr_conn = array();
	//$radioarr_eng = array();
	//$radioarr_exp = array();
    $attributes_heading = 'size="18"';
    $attributes_radio_text = 'size="11"';


	$mform->addElement('header', 'infoHeader', get_string('contextInformation', 'local_rubricrepo_sgannon1'));
	$mform->setExpanded('infoHeader');
	$mform->addElement('textarea', 'markerName', get_string('graderName', 'local_rubricrepo_sgannon1'),'wrap="virtual" rows="1" cols="30" resize="none" style="resize:none"');
    $mform->setDefault('markerName',get_string('defaultGrader', 'local_rubricrepo_sgannon1'));
	$mform->addElement('textarea', "1", get_string('subject', 'local_rubricrepo_sgannon1'),'wrap="virtual" rows="1" cols="60" resize="none" style="resize:none"');


    $mform->addElement('header', 'header0', get_string('studentInfo', 'local_rubricrepo_sgannon1'));
    $mform->setExpanded('header0');	
	$mform->addElement('text', 'student_name', get_string('student', 'local_rubricrepo_sgannon1'));
	$mform->addElement('text', 'student_email', get_string('email', 'local_rubricrepo_sgannon1'));
	$mform->addElement('header', 'header1', get_string('writing', 'local_rubricrepo_sgannon1'));
	$mform->setExpanded('header1',false);
	$mform->addElement('static', 'proWriting', get_string('pros', 'local_rubricrepo_sgannon1'), get_string('empty', 'local_rubricrepo_sgannon1'));	
	$mform->addElement('advcheckbox', 'test1', null, get_string('writing1', 'local_rubricrepo_sgannon1'), array('group'=>1),array('test1'));
    $mform->addElement('advcheckbox', 'test2', null, get_string('writing2', 'local_rubricrepo_sgannon1'), array('group'=>1),array('test1'));
	$mform->addElement('advcheckbox', 'test3', null, get_string('writing3', 'local_rubricrepo_sgannon1'), array('group'=>1),array('test1'));
	$mform->addElement('advcheckbox', 'test4', null, get_string('writing4', 'local_rubricrepo_sgannon1'), array('group'=>1),array('test1'));
	$mform->addElement('advcheckbox', 'test5', null, get_string('writing5', 'local_rubricrepo_sgannon1'), array('group'=>1),array('test1'));
	$mform->addElement('advcheckbox', 'test6', null, get_string('writing6', 'local_rubricrepo_sgannon1'), array('group'=>1),array('test1'));
	$mform->addElement('advcheckbox', 'test7', null, get_string('writing7', 'local_rubricrepo_sgannon1'), array('group'=>1),array('test1'));
	$mform->addElement('advcheckbox', 'test8', null, get_string('writing8', 'local_rubricrepo_sgannon1'), array('group'=>1),array('test1'));
	$this->add_checkbox_controller(1);

	//$mform->addElement('static', 'test_static2', get_string('writing', ' local_rubricrepo_sgannon1'), get_string('writing_neg', ' local_rubricrepo_sgannon1'));
	$mform->addElement('static', 'negWriting', get_string('cons', ' local_rubricrepo_sgannon1'), get_string('empty', ' local_rubricrepo_sgannon1'));
	$mform->addElement('advcheckbox', 'test9', null, get_string('writing9', ' local_rubricrepo_sgannon1'), array('group'=>2));
	$mform->addElement('advcheckbox', 'test10', null, get_string('writing10', ' local_rubricrepo_sgannon1'), array('group'=>2));
	$mform->addElement('advcheckbox', 'test11', null, get_string('writing11', ' local_rubricrepo_sgannon1'), array('group'=>2));
	$mform->addElement('advcheckbox', 'test12', null, get_string('writing12', ' local_rubricrepo_sgannon1'), array('group'=>2));
	$mform->addElement('advcheckbox', 'test13', null, get_string('writing13', ' local_rubricrepo_sgannon1'), array('group'=>2));
	$mform->addElement('advcheckbox', 'test14', null, get_string('writing14', ' local_rubricrepo_sgannon1'), array('group'=>2));
	$this->add_checkbox_controller(2);

	$mform->addElement('header', 'header2', get_string('connections', ' local_rubricrepo_sgannon1'));
	$mform->setExpanded('header2',false);
	//$mform->addElement('static', 'test_static3', get_string('connections', ' local_rubricrepo_sgannon1'), get_string('connections_pos', ' local_rubricrepo_sgannon1'));
	$mform->addElement('static', 'proConnections', get_string('pros', ' local_rubricrepo_sgannon1'), get_string('empty', ' local_rubricrepo_sgannon1'));	
	$mform->addElement('advcheckbox', 'c1', null, get_string('connections1', ' local_rubricrepo_sgannon1'), array('group'=>3));
	$mform->addElement('advcheckbox', 'c2', null, get_string('connections2', ' local_rubricrepo_sgannon1'), array('group'=>3));
	$mform->addElement('advcheckbox', 'c3', null, get_string('connections3', ' local_rubricrepo_sgannon1'), array('group'=>3));
	$mform->addElement('advcheckbox', 'c4', null, get_string('connections4', ' local_rubricrepo_sgannon1'), array('group'=>3));
	$mform->addElement('advcheckbox', 'c5', null, get_string('connections5', ' local_rubricrepo_sgannon1'), array('group'=>3));
	$mform->addElement('advcheckbox', 'c6', null, get_string('connections6', ' local_rubricrepo_sgannon1'), array('group'=>3));
	$this->add_checkbox_controller(3);

    $mform->addElement('static', 'negConnections', get_string('cons', ' local_rubricrepo_sgannon1'), get_string('empty', ' local_rubricrepo_sgannon1'));
	//$mform->addElement('static', 'test_static4', get_string('connections', ' local_rubricrepo_sgannon1'), get_string('connections_neg', ' local_rubricrepo_sgannon1'));
	$mform->addElement('advcheckbox', 'c7', null, get_string('connections7', ' local_rubricrepo_sgannon1'), array('group'=>4));
	$mform->addElement('advcheckbox', 'c8', null, get_string('connections8', ' local_rubricrepo_sgannon1'), array('group'=>4));
	$mform->addElement('advcheckbox', 'c9', null, get_string('connections9', ' local_rubricrepo_sgannon1'), array('group'=>4));
	$mform->addElement('advcheckbox', 'c10', null, get_string('connections10', ' local_rubricrepo_sgannon1'), array('group'=>4));
	$mform->addElement('advcheckbox', 'c11', null, get_string('connections11', ' local_rubricrepo_sgannon1'), array('group'=>4));
	$this->add_checkbox_controller(4);

	$mform->addElement('header', 'header3',  get_string('engage', ' local_rubricrepo_sgannon1'));
	$mform->setExpanded('header3',false);
		$mform->addElement('static', 'proEngage', get_string('pros', ' local_rubricrepo_sgannon1'), get_string('empty', ' local_rubricrepo_sgannon1'));	
	//$mform->addElement('static', 'test_static5', get_string('engage', ' local_rubricrepo_sgannon1'), get_string('engage_pos', ' local_rubricrepo_sgannon1'));
	$mform->addElement('advcheckbox', 'e1', null, get_string('engage1', ' local_rubricrepo_sgannon1'), array('group'=>5));
	$mform->addElement('advcheckbox', 'e2', null, get_string('engage2', ' local_rubricrepo_sgannon1'), array('group'=>5));
	$mform->addElement('advcheckbox', 'e3', null, get_string('engage3', ' local_rubricrepo_sgannon1'), array('group'=>5));
	$this->add_checkbox_controller(5);

	$mform->addElement('header', 'header4',  get_string('expectations', ' local_rubricrepo_sgannon1'));
	$mform->setExpanded('header4',false);
	$mform->addElement('static', 'proExpectations', get_string('pros', ' local_rubricrepo_sgannon1'), get_string('empty', ' local_rubricrepo_sgannon1'));	
	//$mform->addElement('static', 'test_static6', get_string('expectations', ' local_rubricrepo_sgannon1'), get_string('expectations_pos', ' local_rubricrepo_sgannon1'));
	$mform->addElement('advcheckbox', 'ex1', null, get_string('expectations1', ' local_rubricrepo_sgannon1'), array('group'=>6));
	$mform->addElement('advcheckbox', 'ex2', null, get_string('expectations2', ' local_rubricrepo_sgannon1'), array('group'=>6));
	$mform->addElement('advcheckbox', 'ex3', null, get_string('expectations3', ' local_rubricrepo_sgannon1'), array('group'=>6));
	$this->add_checkbox_controller(6);

    $mform->addElement('static', 'negExpectations', get_string('cons', ' local_rubricrepo_sgannon1'), get_string('empty', ' local_rubricrepo_sgannon1'));
	//$mform->addElement('static', 'test_static7', get_string('expectations', ' local_rubricrepo_sgannon1'), get_string('expectations_neg', ' local_rubricrepo_sgannon1'));
	$mform->addElement('advcheckbox', 'ex4', null, get_string('expectations4', ' local_rubricrepo_sgannon1'), array('group'=>7));
	$mform->addElement('advcheckbox', 'ex5', null, get_string('expectations5', ' local_rubricrepo_sgannon1'), array('group'=>7));
	$mform->addElement('advcheckbox', 'ex6', null, get_string('expectations6', ' local_rubricrepo_sgannon1'), array('group'=>7));
	$this->add_checkbox_controller(7);
	//$mform->addGroup($radioarr_w, 'advcheckbox', '', array("<br>"), false);
	//$mform->addGroup($radioarr_conn, 'radioarr_conn', '', array("<br>"), false);
	//$mform->addGroup($radioarr_eng, 'radioarr_eng', '', array("<br>"), false);
	//$mform->addGroup($radioarr_exp, 'radioarr_exp', '', array("<br>"), false);
	$mform->addElement('header', 'header5',  get_string('freetext', ' local_rubricrepo_sgannon1'));
	$mform->setExpanded('header5');
	$mform->addElement('textarea', 'intro', get_string('empty', ' local_rubricrepo_sgannon1'), 'wrap="virtual" rows="10" cols="60" resize="none" style="resize:none"');
	$this->add_action_buttons();

	}
/*
	function validation($data, $files) {
    	$errors = parent::validation($data, $files);

     	if (empty($data['student_email'])) {
    		$errors['student_email'] = get_string('field_required', 'local_feedback_ec10');
    	}

    	return $errors;
    }

    **/
};

?>
