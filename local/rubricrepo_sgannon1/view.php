<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the feedback form
 *
 * @package     local
 * @subpackage  rubricrepo_sgannon1
 * @copyright   Eric Cheng ec10@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
require_once('../../config.php');
global $CFG, $PAGE;

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title('Form name');
$PAGE->set_heading('Form name');
$PAGE->set_url($CFG->wwwroot.'/local/rubricrepo_sgannon1/view.php');
echo $OUTPUT->header();
**/


require_once('../../config.php');
global $CFG, $PAGE, $USER;
require_login();

require_capability('local/rubricrepo_sgannon1:add', context_system::instance());
require_once($CFG->dirroot.'/local/rubricrepo_sgannon1/feedback_form.php');

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('pluginname', 'local_rubricrepo_sgannon1'));
$PAGE->set_heading(get_string('pluginname', 'local_rubricrepo_sgannon1'));
$PAGE->set_url($CFG->wwwroot.'/local/rubricrepo_sgannon1/view.php');

    	$username = $USER->username;
			echo $username;
/*
$feedback_form = new create_feedback_instance();


if ($feedback_form->is_cancelled()) {
	redirect($CFG->wwwroot.'/local/rubricrepo_sgannon1/view.php');
} elseif ($data = $feedback_form->get_data()) {

	redirect($CFG->wwwroot.'/local/rubricrepo_sgannon1/email.php?id='.$data->c1);

} else {
	echo $OUTPUT->header();
	$feedback_form->display();
}
*/
?>
