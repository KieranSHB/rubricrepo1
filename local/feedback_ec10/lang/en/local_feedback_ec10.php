<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'feedback_ec10', language 'en'
 *
 * @package     local
 * @subpackage  feedback_ec10
 * @copyright   Eric Cheng ec10@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['menuoption'] = 'ec10 Feedback Plugin (DEV build)';
$string['pluginname'] = 'ec10 Feedback Plugin (DEV)';
$string['feedback_ec10:add'] = 'You do not have permission to use the feedback form';
$string['student'] = 'Student Name';
$string['email'] = 'Student E-mail';
$string['subject'] = 'Discussion Subject';
$string['freetext'] = 'Comments';
$string['field_required'] = 'This is a required field';
$string['search'] = 'Search';
$string['selectform'] = 'Select a form to use';

/**
 * The following strings are used to define errors to the user.
 */
$string['error'] = 'An error occurred';
$string['noformtext'] = 'The form you have selected does not exist';

/**
 * The code below was used to test, play around with, and familiarize myself with the Moodle architecture.
 * It is for generation of a static form with pre-defined strings from the /lang/en folder of the plug-in.
 * New code is now capable of creating the form dynamically based on the form information in the database.
 * These strings are kept so I don't need to re-type it all.
$string['writing'] = 'Writing';
$string['writing_pos'] = 'Positive feedback about the writing style';
$string['writing_neg'] = 'Feedback about writing items to improve on';
$string['writing1'] = 'Writes creatively';
$string['writing2'] = 'Presents ideas thoughtfully';
$string['writing3'] = 'Writes clearly and concisely';
$string['writing4'] = 'Writes correctly (free of spelling and grammatical errors)';
$string['writing5'] = 'Organizes scholarly contribution';
$string['writing6'] = 'Synthesizes of assigned reading/resournce';
$string['writing7'] = 'Uses few direct quotes';
$string['writing8'] = 'Presents of unique ideas/conceptualizations';
$string['writing9'] = 'Engage the reader with a story, metaphor or analogy';
$string['writing10'] = 'Organize the post into clear themes';
$string['writing11'] = 'Demonstrate synthesis of all assigned readings/resources';
$string['writing12'] = 'Paraphrase or cite more often and use direct quotes less often';
$string['writing13'] = 'Check your grammar and spelling (watch for typos)';
$string['writing14'] = 'Use a more academic tone';
$string['connections'] = 'Connections';
$string['connections_pos'] = 'Positive feedback about the ability of the writer to make connections between the reading(s) and other materials';
$string['connections_neg'] = 'Feedback about items to improve on in order make connections between the reading(s) and other materials';
$string['connections1'] = 'Makes appropriate connections to assigned readings/resources';
$string['connections2'] = 'Makes clear connections to other research (scholarly articles, conference proceedings)';
$string['connections3'] = 'Makes appropriate connections to other resources (books, curriculum, documents, school policies, websites, reports, etc.)';
$string['connections4'] = 'Includes other sources of practical expertise (blogs, wikis, videos, etc.)';
$string['connections5'] = 'Makes clear connections to personal experiences';
$string['connections6'] = 'Makes clear connections to professional experiences';
$string['connections7'] = 'Make connections to all assigned readings/resources';
$string['connections8'] = 'Search for research related to the topic to enhance your scholarly contribution';
$string['connections9'] = 'Include links to other resources (books, curriculum documents, school policies, websites, reports, etc)';
$string['connections10'] = 'Make clear connections to personal experiences';
$string['connections11'] = 'Make clear connections to professional experiences';
$string['engage'] = 'Engaging and Inviting';
$string['engage_pos'] = 'Positive feedback about how engaging the writer was';
$string['engage1'] = 'Invites others to make connections';
$string['engage2'] = 'Asks questions to promote discussion';
$string['engage3'] = 'Poses problems or challenges to enhance discussions';
$string['expectations'] = 'Meets Assignment Expectations';
$string['expectations_pos'] = 'Positive feedback about meeting assignment expectations';
$string['expectations_neg'] = 'Feedback about items to improve on for meeting assignment expectations';
$string['expectations1'] = 'Meets word limit';
$string['expectations2'] = 'Uses in-text citatiions correctly (in APA)';
$string['expectations3'] = 'Includes a complete and correctly formatted reference list (in APA)';
$string['expectations4'] = 'Keep to the word limit';
$string['expectations5'] = 'Use your APA manual to help you conplete correctly formatted in-text citations';
$string['expectations6'] = 'Use your APA manual to help you complete a correctly formatted reference list';
*/
?>
