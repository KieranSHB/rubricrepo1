<?php
/**
 * Handles upgrading the plug-in
 *
 * @package    local
 * @subpackage feedback_ec10
 * @copyright  Eric Cheng ec10@ualberta.ca
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function xmldb_local_feedback_ec10_upgrade($oldversion) {

    global $CFG, $USER, $DB, $OUTPUT;

    require_once($CFG->dirroot.'/lib/db/upgradelib.php');

    $dbman = $DB->get_manager();

    /**
     * Version 2015110301 adds tables for saving the completed form data 
     */
    if ($oldversion < 2015110301) {

        // Define table saved_form to be created.
        $table = new xmldb_table('saved_form');

        // Adding fields to table saved_form.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);

        // Adding keys to table saved_form.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for saved_form.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table saved_category to be created.
        $table = new xmldb_table('saved_category');

        // Adding fields to table saved_category.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('savedform_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('category_ref_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table saved_category.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('savedform_id', XMLDB_KEY_FOREIGN, array('savedform_id'), 'saved_form', array('id'));
        $table->add_key('category_ref_id', XMLDB_KEY_FOREIGN, array('category_ref_id'), 'category', array('id'));

        // Conditionally launch create table for saved_category.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

         // Define table saved_comment to be created.
        $table = new xmldb_table('saved_comment');

        // Adding fields to table saved_comment.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('savedcategory_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('comment_ref_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table saved_comment.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('savedcategory_id', XMLDB_KEY_FOREIGN, array('savedcategory_id'), 'saved_category', array('id'));
        $table->add_key('comment_ref_id', XMLDB_KEY_FOREIGN, array('comment_ref_id'), 'comments', array('id'));

        // Conditionally launch create table for saved_comment.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        // Feedback_ec10 savepoint reached.
        upgrade_plugin_savepoint(true, 2015110301, 'local', 'feedback_ec10');

    }
}