<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The form used by 'feedback_pranjali' to pick a forum
 *
 * @package     local
 * @subpackage  feedback_pranjali
 * @copyright   Pranjali Pokharel pranjali@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once $CFG->dirroot.'/lib/formslib.php';
//require_once($CFG->libdir.'datalib.php');

class create_forum_list extends moodleform {
    
    function definition() {
        global $CFG, $COURSE, $DB;
        $mform =& $this->_form;
        $attributes_heading = 'size="25"';
        $attributes_subheading='size="18"';
        $mform->addElement('header', 'heading1', get_string('pickforumheading', 'local_feedback_pranjali'), $attributes_heading);
        $mform->addElement('text', 'heading2', get_string('chooseforum', 'local_feedback_pranjali'), $attributes_subheading);
        
        $group1=array();
        $group1[] =& $mform->createElement('checkbox', 'test1', 'null', get_string('writing1','local_feedback_pranjali'));
        
        
        $this->add_action_buttons();
        //Option 1: $course->id and $user->id
        
        /** Option 2
        $name = $_GET['name'];
        echo $name ;*/
    }
    
};

?>