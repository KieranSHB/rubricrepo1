<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the course selection template
 *
 * @package     local
 * @subpackage  feedback_pranjali
 * @copyright   Pranjali Pokharel pranjali@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
 */


global $CFG, $PAGE;
require_once('../../config.php');

require_login();
require_capability('local/feedback_pranjali:add', context_system::instance());
require_once($CFG->dirroot.'/local/feedback_pranjali/feedback_form.php');

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('pluginname', 'local_feedback_pranjali'));
$PAGE->set_heading(get_string('pluginname', 'local_feedback_pranjali'));
$PAGE->set_url($CFG->wwwroot.'/local/feedback_pranjali/feedback.php');
$feedback_form = new create_feedback_instance();

//echo $OUTPUT->header();

/*
* This code is for loading the course list and displaying the contents
* and is responsible for  for redirecting and displaying the header and
* the footer
*  
*/
if ($feedback_form->is_cancelled()) {
	redirect($CFG->wwwroot.'/local/feedback_pranjali/view.php');
} elseif ($data = $feedback_form->get_data()) {
	//$check = $data->test1;
	redirect($CFG->wwwroot.'/local/feedback_pranjali/email.php?id='.$data->test1);
} else {
	//$PAGE->set_title($forum->name);
    //$PAGE->add_body_class('forumtype-'.$forum->type);
    //$PAGE->set_heading($course->fullname);

	echo $OUTPUT->header();
	$feedback_form->display();
	echo $OUTPUT->footer();
}

?>