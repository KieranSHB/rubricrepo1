<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the forum selection template
 *
 * @package     local
 * @subpackage  feedback_pranjali
 * @copyright   Pranjali Pokharel pranjali@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
 */


global $CFG, $PAGE;
require_once('../../config.php');

require_login();
require_capability('local/feedback_pranjali:add', context_system::instance());
require_once($CFG->dirroot.'/local/feedback_pranjali/forum_selection_form.php');

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('pluginname', 'local_feedback_pranjali'));
$PAGE->set_heading(get_string('pluginname', 'local_feedback_pranjali'));
$PAGE->set_url($CFG->wwwroot.'/local/feedback_pranjali/forum_selection.php');
$forumlist_form = new create_forum_list();

//echo $OUTPUT->header();

/*
* This code is for loading the forum list within the selected course and displaying the contents
* and is responsible for  for redirecting and displaying the header and
* the footer
*  
*/
if ($forumlist_form->is_cancelled()) {
	redirect($CFG->wwwroot.'/local/feedback_pranjali/view.php');
} elseif ($data = $forumlist_form->get_data()) {
	$check = $data->test1;
	redirect($CFG->wwwroot.'/local/feedback_pranjali/feedback.php');
} else {
	echo $OUTPUT->header();
	$forumlist_form->display();
	echo $OUTPUT->footer();
}

?>