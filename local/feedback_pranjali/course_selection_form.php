<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The form used by 'feedback_pranjali' to pick a course
 *
 * @package     local
 * @subpackage  feedback_pranjali
 * @copyright   Pranjali Pokharel pranjali@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once $CFG->dirroot.'/lib/formslib.php';
//require_once($CFG->libdir.'datalib.php');

class create_course_list extends moodleform {
    
    function definition() {
        global $CFG, $COURSE, $DB;
        $mform =& $this->_form;
        $attributes_heading = array('size' => '30');
        $attributes =array('size'=>'18', 'maxlength' => '100');
        $mform->addElement('header', 'heading1', get_string('courseheading', 'local_feedback_pranjali'), $attributes_heading);
        $mform->addElement('text', 'heading2', get_string('choosecourse', 'local_feedback_pranjali'), $attributes);
        
        $group1=array();
        $group1[] =& $mform->createElement('checkbox', 'test1', 'null', get_string('writing1','local_feedback_pranjali'));
        $chkbox_array = array();
        $mform->addElement('button', 'intro', get_string('search', 'local_feedback_pranjali'));
        
        $this->add_action_buttons();
        //Option 1: $course->id and $user->id
        
        /** Option 2
        $name = $_GET['name'];
        echo $name ;
        $mform->addElement('text', 'student_name', get_string('student', 'local_feedback_pranjali'), $attributes_heading);
        $mform->addElement('text', 'student_email', get_string('email', 'local_feedback_pranjali'), $attributes_heading);
        $mform->addElement('textarea', 'intro', get_string('freetext', 'local_feedback_pranjali'), 'wrap="virtual" rows="10" cols="60" resize="none" style="resize:none"');

        $mform->addElement('static', 'test_static', get_string('writing', 'local_feedback_pranjali'), get_string('writing_pos', 'local_feedback_pranjali'));
        
        
        $chkbox_array[] = $mform->createElement('advcheckbox', 'test1', null, get_string('writing1', 'local_feedback_pranjali'), array('group'=>1));
        $chkbox_array[] = $mform->createElement('advcheckbox', 'test2', null, get_string('writing2', 'local_feedback_pranjali'), array('group'=>1));
        $chkbox_array[] = $mform->createElement('advcheckbox', 'test3', null, get_string('writing3', 'local_feedback_pranjali'), array('group'=>1));
  
        $mform->addGroup($chkbox_array, 'chkarr', '', array('<br>'), false);
        $this->add_checkbox_controller(1);

        $mform->addElement('static', 'test_static2', get_string('writing', 'local_feedback_pranjali'), get_string('writing_neg', 'local_feedback_pranjali'));
        $mform->addElement('advcheckbox', 'test9', null, get_string('writing9', 'local_feedback_pranjali'), array('group'=>2));
        $mform->addElement('advcheckbox', 'test10', null, get_string('writing10', 'local_feedback_pranjali'), array('group'=>2));
        $this->add_checkbox_controller(2);

        $mform->addElement('static', 'test_static3', get_string('connections', 'local_feedback_pranjali'), get_string('connections_pos', 'local_feedback_pranjali'));
        $mform->addElement('advcheckbox', 'c1', null, get_string('connections1', 'local_feedback_pranjali'), array('group'=>3));
        $this->add_checkbox_controller(3);
*/
        
    }
    
/**
    function validation($data, $files) {
        $errors = parent::validation($data, $files);

        if (empty($data['student_email'])) {
            $errors['student_email'] = get_string('field_required', 'local_feedback_pranjali');
        }

        return $errors;
    }
**/
};

?>