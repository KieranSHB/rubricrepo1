<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The form used by 'feedback_pranjali'
 *
 * @package     local
 * @subpackage  feedback_pranjali
 * @copyright   Pranjali Pokharel pranjali@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
echo $OUTPUT->get_string('writing', 'local_feedback_pranjali');
global $CFG, $DB;
require_once $CFG->dirroot.'/lib/formslib.php';
//require_once($CFG->libdir.'datalib.php');

class create_feedback_instance extends moodleform {
    
    function definition() {
        $mform = $this->_form;
        $chkbox_array = array();
        $attributes_heading = 'size="20"';
        $attributes_radio_text='size="12"';
        
        
        $mform->addElement('text', 'subject', get_string('subject', 'local_feedback_pranjali'), $attributes_heading);
        //Option 1: $course->id and $user->id
        
        /** Option 2
        $name = $_GET['name'];
        echo $name ;*/
        $mform->addElement('text', 'student_name', get_string('student', 'local_feedback_pranjali'), $attributes_heading);
        $mform->addElement('text', 'student_email', get_string('email', 'local_feedback_pranjali'), $attributes_heading);
        $mform->addElement('textarea', 'intro', get_string('freetext', 'local_feedback_pranjali'), 'wrap="virtual" rows="10" cols="60" resize="none" style="resize:none"');

        $mform->addElement('static', 'test_static', get_string('writing', 'local_feedback_pranjali'), get_string('writing_pos', 'local_feedback_pranjali'));
        $group1=array();
        $group1[] =& $mform->createElement('checkbox', 'test1', 'null', get_string('writing1','local_feedback_pranjali'));
        $chkbox_array[] = $mform->createElement('advcheckbox', 'test1', null, get_string('writing1', 'local_feedback_pranjali'), array('group'=>1));
        $chkbox_array[] = $mform->createElement('advcheckbox', 'test2', null, get_string('writing2', 'local_feedback_pranjali'), array('group'=>1));
        $chkbox_array[] = $mform->createElement('advcheckbox', 'test3', null, get_string('writing3', 'local_feedback_pranjali'), array('group'=>1));
        $chkbox_array[] = $mform->createElement('advcheckbox', 'test4', null, get_string('writing4', 'local_feedback_pranjali'), array('group'=>1));
        $chkbox_array[] = $mform->createElement('advcheckbox', 'test5', null, get_string('writing5', 'local_feedback_pranjali'), array('group'=>1));
        $chkbox_array[] = $mform->createElement('advcheckbox', 'test6', null, get_string('writing6', 'local_feedback_pranjali'), array('group'=>1));
        $chkbox_array[] = $mform->createElement('advcheckbox', 'test7', null, get_string('writing7', 'local_feedback_pranjali'), array('group'=>1));
        $chkbox_array[] = $mform->createElement('advcheckbox', 'test8', null, get_string('writing8', 'local_feedback_pranjali'), array('group'=>1));
        $mform->addGroup($chkbox_array, 'chkarr', '', array('<br>'), false);
        $this->add_checkbox_controller(1);

        $mform->addElement('static', 'test_static2', get_string('writing', 'local_feedback_pranjali'), get_string('writing_neg', 'local_feedback_pranjali'));
        $mform->addElement('advcheckbox', 'test9', null, get_string('writing9', 'local_feedback_pranjali'), array('group'=>2));
        $mform->addElement('advcheckbox', 'test10', null, get_string('writing10', 'local_feedback_pranjali'), array('group'=>2));
        $mform->addElement('advcheckbox', 'test11', null, get_string('writing11', 'local_feedback_pranjali'), array('group'=>2));
        $mform->addElement('advcheckbox', 'test12', null, get_string('writing12', 'local_feedback_pranjali'), array('group'=>2));
        $mform->addElement('advcheckbox', 'test13', null, get_string('writing13', 'local_feedback_pranjali'), array('group'=>2));
        $mform->addElement('advcheckbox', 'test14', null, get_string('writing14', 'local_feedback_pranjali'), array('group'=>2));
        $this->add_checkbox_controller(2);

        $mform->addElement('static', 'test_static3', get_string('connections', 'local_feedback_pranjali'), get_string('connections_pos', 'local_feedback_pranjali'));
        $mform->addElement('advcheckbox', 'c1', null, get_string('connections1', 'local_feedback_pranjali'), array('group'=>3));
        $mform->addElement('advcheckbox', 'c2', null, get_string('connections2', 'local_feedback_pranjali'), array('group'=>3));
        $mform->addElement('advcheckbox', 'c3', null, get_string('connections3', 'local_feedback_pranjali'), array('group'=>3));
        $mform->addElement('advcheckbox', 'c4', null, get_string('connections4', 'local_feedback_pranjali'), array('group'=>3));
        $mform->addElement('advcheckbox', 'c5', null, get_string('connections5', 'local_feedback_pranjali'), array('group'=>3));
        $mform->addElement('advcheckbox', 'c6', null, get_string('connections6', 'local_feedback_pranjali'), array('group'=>3));
        $this->add_checkbox_controller(3);

        $mform->addElement('static', 'test_static4', get_string('connections', 'local_feedback_pranjali'), get_string('connections_neg', 'local_feedback_pranjali'));
        $mform->addElement('advcheckbox', 'c7', null, get_string('connections7', 'local_feedback_pranjali'), array('group'=>4));
        $mform->addElement('advcheckbox', 'c8', null, get_string('connections8', 'local_feedback_pranjali'), array('group'=>4));
        $mform->addElement('advcheckbox', 'c9', null, get_string('connections9', 'local_feedback_pranjali'), array('group'=>4));
        $mform->addElement('advcheckbox', 'c10', null, get_string('connections10', 'local_feedback_pranjali'), array('group'=>4));
        $mform->addElement('advcheckbox', 'c11', null, get_string('connections11', 'local_feedback_pranjali'), array('group'=>4));
        $this->add_checkbox_controller(4);

        $mform->addElement('static', 'test_static5', get_string('engage', 'local_feedback_pranjali'), get_string('engage_pos', 'local_feedback_pranjali'));
        $mform->addElement('advcheckbox', 'e1', null, get_string('engage1', 'local_feedback_pranjali'), array('group'=>5));
        $mform->addElement('advcheckbox', 'e2', null, get_string('engage2', 'local_feedback_pranjali'), array('group'=>5));
        $mform->addElement('advcheckbox', 'e3', null, get_string('engage3', 'local_feedback_pranjali'), array('group'=>5));
        $this->add_checkbox_controller(5);

        $mform->addElement('static', 'test_static6', get_string('expectations', 'local_feedback_pranjali'), get_string('expectations_pos', 'local_feedback_pranjali'));
        $mform->addElement('advcheckbox', 'ex1', null, get_string('expectations1', 'local_feedback_pranjali'), array('group'=>6));
        $mform->addElement('advcheckbox', 'ex2', null, get_string('expectations2', 'local_feedback_pranjali'), array('group'=>6));
        $mform->addElement('advcheckbox', 'ex3', null, get_string('expectations3', 'local_feedback_pranjali'), array('group'=>6));
        $this->add_checkbox_controller(6);

        $mform->addElement('static', 'test_static7', get_string('expectations', 'local_feedback_pranjali'), get_string('expectations_neg', 'local_feedback_pranjali'));
        $mform->addElement('advcheckbox', 'ex4', null, get_string('expectations4', 'local_feedback_pranjali'), array('group'=>7));
        $mform->addElement('advcheckbox', 'ex5', null, get_string('expectations5', 'local_feedback_pranjali'), array('group'=>7));
        $mform->addElement('advcheckbox', 'ex6', null, get_string('expectations6', 'local_feedback_pranjali'), array('group'=>7));
        $this->add_checkbox_controller(7);
        

        $this->add_action_buttons();
        /*first text box ie div 1
        $mform->addElement('text', 'name', get_string('student', 'local_feedback_pranjali'), $attributes) ;
        $mform->addElement('text', 'email', get_string('email', 'local_feedback_pranjali'), $attributes);
        
        $mform->addElement('advcheckbox', 'test1', null, get_string('writing1', 'local_feedback_pranjali'), array('group'=>1));
        $mform->addElement('advcheckbox', 'test2', null, get_string('writing2', 'local_feedback_pranjali'), array('group'=>1));
        $mform->addElement('advcheckbox', 'test3', null, get_string('writing3', 'local_feedback_pranjali'), array('group'=>1));
        $mform->addElement('advcheckbox', 'test4', null, get_string('writing4', 'local_feedback_pranjali'), array('group'=>1));
        $mform->addElement('advcheckbox', 'test5', null, get_string('writing5', 'local_feedback_pranjali'), array('group'=>1));
        $mform->addElement('advcheckbox', 'test6', null, get_string('writing6', 'local_feedback_pranjali'), array('group'=>1));
        $mform->addElement('advcheckbox', 'test7', null, get_string('writing7', 'local_feedback_pranjali'), array('group'=>1));
        $mform->addElement('advcheckbox', 'test8', null, get_string('writing8', 'local_feedback_pranjali'), array('group'=>1));
        $this->add_checkbox_controller(1);
        
        $mform->addElement('button', 'intro', get_string('sendfeedback', 'local_feedback_pranjali'), $attributes);*/
    }
    
/**
    function validation($data, $files) {
        $errors = parent::validation($data, $files);

        if (empty($data['student_email'])) {
            $errors['student_email'] = get_string('field_required', 'local_feedback_pranjali');
        }

        return $errors;
    }
**/
};

?>